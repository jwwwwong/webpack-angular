var webpack = require("webpack");

module.exports = {
    entry: {
        app: './src/index.js',
        vendor: ['angular']
    },
    output: {
        path: './dist',
        filename: 'bundle.js',
        publicPath: "/dist/"
    },
    devServer: {
        inline: true,
        port: 3334
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    plugins: ['transform-decorators-legacy' ],
                    presets: ['es2015', "stage-2"]
                }
            },
            {
                test: /\.scss$/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name:"vendor",
            filename: "vendor.js"
        })
    ]
}
