import angular from 'angular'
import routes from './routes'
import ngRoute from 'angular-route'
import ListWidget from './components/ListWidget'
import AboutWidget from './components/AboutWidget'

angular.module('app', ['ngRoute'])
    .config(routes)
    .directive('listWidget', ()=> new ListWidget)
    .directive('aboutWidget', ()=> new AboutWidget)