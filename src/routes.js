routing.$inject = ['$routeProvider', '$locationProvider'];

export default function routing($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', { template: '<list-widget></list-widget>' })
        .when('/about', { template: '<about-widget></about-widget>' })

    $locationProvider.html5Mode(true) // for supported browsers the default #! will not appear in the URLs
}