export default class ListWidget {
    constructor() {
        this.restrict = 'E'
        this.scope = {}
        this.controllerAs = 'ctrl'
        this.template = this.getTemplate()
        this.controller = ListWidgetController
    }

    getTemplate() {
        return `
        <div>
            <h2>List Widget</h2>
            <div>           
                <a href='/about'>About</a>
            </div>
            <ul>
                <li ng-repeat='item in ctrl.items'>{{ item }}</li>
            </ul>
            <div>
                <button ng-click='ctrl.addItem()'>Add Item</button>
            </div>
        </div>
        `
    }
}

class ListWidgetController {
    constructor($timeout) {
        this.items = [1, 2, 3]
        this.$timeout = $timeout
        this.example()
    }

    example() {
        this.$timeout(function() {
            console.log("test")
        })
    }

    addItem() {
        this.items.push(this.items.length + 1)
    }
}

ListWidget.$inject = ['$timeout']