export default class AboutWidget {
    constructor() {
        this.restrict = 'E'
        this.scope = {}
        this.controllerAs = 'ctrl'
        this.template = this.getTemplate()
        this.controller = AboutWidgetController
    }

    getTemplate() {
        return `
        <div>
            <h2>About Widget</h2>
            <div>           
                <a href='/'>Home</a>
            </div>
        </div>
        `
    }
}

class AboutWidgetController {
    constructor() {
    }
}